# Crote

Versioned Markdown notes in your $EDITOR.

### USAGE

      $ crote [options] [command]

### COMMANDS

* `l`, `list`: List existing notes.
* `e`, `edit FILE`: Edit note in FILE.
* `d`, `rm`, `delete FILE`: Delete FILE.

### OPTIONS

* `-h`, `--help`: Print this help
* `-t`, `--template FILE`: Creates a new note filling copying over the content from FILE.

### DEPENDENCIES

* `git`: Make sure user.email and user.name are defined.
* `$HOME`: env variable
* `$EDITOR`: env variable

### FILE COMPLETION

To enable file completion add the following to your ~/.bashrc or ~/.zshrc

      # Crote completion function
      _crote() {
        COMPREPLY=(`ls ~/.crote`)
      }
      complete -F _crote crote
