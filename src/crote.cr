require "option_parser"

Crote.init

template = nil
OptionParser.parse! do |parser|
  parser.on("-h", "--help", "Show help") { puts Crote.usage }
  parser.on("-t FILE", "--template=FILE", "Create note from a template") { |file| template = file }
end

command = ARGV.shift { "list" }
Crote.run(command, template)

module Crote
  extend self

  @@missing_var : Proc(String, String) = ->(env_var : String) { abort "$#{env_var} variable required." }
  @@home        : String               = ENV.fetch("HOME",   &@@missing_var)
  @@editor      : String               = ENV.fetch("EDITOR", &@@missing_var)

  def usage
    <<-_USAGE
    Crote

    Versioned Markdown notes in your $EDITOR.

    USAGE

      $ crote [options] [command]

    COMMANDS

      l, list               List existing notes.
      e, edit FILE          Edit note in FILE.
      d, rm, delete FILE    Delete FILE.

    OPTIONS

      -h, --help             Print this help
      -t, --template FILE    Creates a new note filling copying over the content from FILE.

    DEPENDENCIES

      git        Make sure user.email and user.name are defined.
      $HOME      env variable
      $EDITOR    env variable

    FILE COMPLETION

    To enable file completion add the following to your ~/.bashrc or ~/.zshrc

      # Crote completion function
      _crote() {
        COMPREPLY=(`ls #{crote_dir}`)
      }
      complete -F _crote crote

    _USAGE
  end

  def init
    Dir.cd(crote_dir)

    Process.run("git", ["init"]) unless Dir.exists?(".git")
  end

  def run(command, template)
    case command
    when "l", "list"
      Process.run("ls", output: true)
    when "e", "edit"
      file = normalize_name(ARGV.shift { abort "Filename required." })

      if template
        template = normalize_name(template)

        edit(file, template)
      else
        edit(file)
      end

      commit(file)
    when "d", "delete", "rm"
      file = normalize_name(ARGV.shift { abort "Filename required." })

      delete(file)
    else
      puts usage
    end
  end

  def crote_dir
    dir = "#{@@home}/.crote"
    Dir.mkdir(dir) unless Dir.exists?(dir)
    dir
  end

  def edit(file)
    Process.run("touch", [file])
    launch_editor(file)
  end

  def edit(file, template)
    Process.run("cp", [template, file])
    launch_editor(file)
  end

  def launch_editor(file)
    Process.fork { Process.exec(@@editor, [file], input: true, output: true) }.wait
  end

  def commit(file)
    Process.run("git", ["add", file])
    Process.run("git", ["commit", "-m", "Edit #{file}"])
  end

  def delete(file)
    Process.run("git", ["rm", file])
    Process.run("git", ["commit", "-m", "Delete #{file}"])
  end

  def normalize_name(file)
    /.+\.(md|markdown)$/.match(file) ? file : file + ".md"
  end
end

