SRC = $(wildcard src/*.cr)
CRYSTAL = crystal

build: bin
	$(CRYSTAL) build -o bin/crote $(SRC)

bin:
	mkdir -p $@

install: build
	cp bin/crote ~/bin
